unit MainForm;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.Edit, FMX.Layouts, FMX.Memo, FMX.StdCtrls,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdGlobal, Posix.Systime, Posix.pthread, Dateutils, Posix.sched, Posix.systypes;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    IP: TEdit;
    IDC: TIdTCPClient;
    Button_Send: TButton;
    Timer1: TTimer;
    Switch1: TSwitch;
    Port: TEdit;
    Command: TEdit;
    Button1: TButton;
    procedure scrollmemo;
    procedure sendCommand(const s:String);
    function readanswer:string;
    procedure IDCConnected(Sender: TObject);
    procedure Button_SendClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Switch1Switch(Sender: TObject);
    procedure IDCDisconnected(Sender: TObject);
    procedure CommandKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.scrollmemo;
begin
  memo1.SelStart := memo1.Text.Length;
  memo1.SelLength := 0;
  memo1.ScrollBy(0, memo1.Lines.Count);
end;

procedure TForm1.sendCommand(const s:String);
begin
  IDC.IOHandler.Write(s+#13);
end;

procedure TForm1.Button1Click(Sender: TObject);
//  timer1.Enabled:=not timer1.Enabled;
var TV,TV1:Timeval;
    tvdiv,i,Policy:integer;

var
  P: sched_param;
  J: sched_param;
  ThreadID: TThreadID;
begin

    ThreadID:=0;
    ThreadID:=GetCurrentThreadId;
    pthread_getschedparam(pthread_t(ThreadID), Policy, J);
    P.sched_priority := sched_get_priority_max(SCHED_FIFO);

   memo1.Lines.Add(inttostr(pthread_setschedparam(pthread_t(ThreadID), SCHED_FIFO, P)));

for i := 0 to 150 do
begin
 gettimeofday(TV, nil);
{ sendCommand('l');
 readAnswer;               }
 gettimeofday(TV1, nil);
 tvdiv:=TV1.tv_usec-TV.tv_usec;
 if tvdiv<0 then tvdiv:=1000000+tvdiv;
 while tvdiv<100000 do
 begin
   gettimeofday(TV1, nil);
   tvdiv:=TV1.tv_usec-TV.tv_usec;
   if tvdiv<0 then tvdiv:=1000000+tvdiv;
 end;

 memo1.Lines.Add(inttostr(tvdiv));
 scrollmemo;
end;
//timer1.Enabled:=not timer1.Enabled;
 // memo1.Lines.Add(inttostr(ord(IDC.IOHandler.InputBufferIsEmpty)));
end;

procedure TForm1.Button_SendClick(Sender: TObject);
begin
  sendCommand(Command.Text);
  memo1.Lines.Add(readAnswer);
  scrollmemo;
//  memo1.Repaint;
  Command.SelectAll;
  Command.DeleteSelection;
end;

function TForm1.readAnswer:string;
var rcvByte:byte;
begin
  result:='';
  rcvByte:=IDC.IOHandler.ReadByte;
  while rcvByte<>13 do
  begin
    result:=result+chr(rcvByte);
    rcvByte:=IDC.IOHandler.ReadByte;
  end;
end;

procedure TForm1.Switch1Switch(Sender: TObject);
begin
  if Switch1.IsChecked then
  begin
    IDC.Host:=IP.Text;
    IDC.Port:=strtoint(Port.Text);
    try
      IDC.Connect;
    except
      on E : Exception do
      begin
        memo1.Lines.Add(E.ClassName+'message: '+E.Message);
        Switch1.IsChecked:=false;
      end;
    end;
    IDC.IOHandler.ReadTimeout:=1000;
  end else
  begin
    timer1.Enabled:=false;
    IDC.Disconnect;
  end;

end;

procedure TForm1.CommandKeyDown(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if key=13 then
    Button_SendClick(self);
end;

procedure TForm1.IDCConnected(Sender: TObject);
begin
  memo1.Lines.Add('Connected');
  sleep(300);
  sendCommand('q');
  memo1.Lines.Add(readAnswer);
  sendCommand('v');
  memo1.Lines.Add(readAnswer);
  sendCommand('d');
  memo1.Lines.Add(readAnswer);
end;

procedure TForm1.IDCDisconnected(Sender: TObject);
begin
  memo1.Lines.Add('Disconnected');
  scrollmemo;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var TV,TV1:Timeval;
    tvdiv:integer;
begin

 gettimeofday(TV, nil);
 sendCommand('l');
 readAnswer;
 gettimeofday(TV1, nil);
 tvdiv:=TV1.tv_usec-TV.tv_usec;
 if tvdiv<0 then tvdiv:=1000000+tvdiv;
 while tvdiv<10000 do
 begin
   gettimeofday(TV1, nil);
   tvdiv:=TV1.tv_usec-TV.tv_usec;
   if tvdiv<0 then tvdiv:=1000000+tvdiv;
 end;

 memo1.Lines.Add(inttostr(tvdiv));
 scrollmemo;
 // memo1.Lines.Add(inttostr(ord(IDC.IOHandler.InputBufferIsEmpty)));
end;

end.
