unit ButtonThread_TIGAC;

interface

uses
  System.Classes, SysUtils, Posix.Systime, System.Generics.Collections;

type
  TDCStateProc = procedure of object;
  TDCStateProc1 = pointer;
  //TDCStateDo = TDictionary<string, TDCStateProc>;

  TButtonThread_TIGAC = class(TThread)
  protected
    procedure Execute; override;
    procedure CheckParams;
    procedure ReadAnsw;
    procedure AC_I_increment;
    procedure AC_PWM_start;
    procedure AC_GasOpen_ResetT;
    procedure AC_I_decrement_ResetT1;
    procedure AC_GasClose;
    procedure AC_PWM_stop;
    procedure AC_AC_start;
    function sendCommand(cmd: String): String;

    procedure OpenGas;
    procedure CloseGas;
    procedure StartPWM;
    procedure StartAC;
    procedure StopPWM;
    procedure SetI(I: integer);
    procedure SetBal(Bal: integer);
    procedure SetFreq(Freq: integer);
    procedure SetIClean(I: integer);

  var
    FDCStateProc: TDCStateProc;

    property DCState: TDCStateProc read FDCStateProc write FDCStateProc;
    // property StateProc: TDCStateProc;

  var
    //DCStateDo: TDCStateDo;
    bufsize: integer;
    answer: string;
    cmdStatus: boolean;
    rcvByte: byte;
    TV_: Timeval;
    TV: Timeval;
    TV1: Timeval;
    TV_I: Timeval;
    tvdiv: integer;
    isPWM: boolean;
    t, t1, t_I: integer;
    Ic, U, isBut, isGas, isGas1: integer;

  I_Min: integer;
  I_Max: integer;
  I_Max_clean: integer;
  I_Min_clean: integer;
  tCloseGas: integer;    // время, через которое выключается газ при минимальном токе
  tStartPWM: integer;     // время, через которое стартует ШИМ
  tImax: integer;        // время, за которое ток должен достичь максимума
  tImin: integer;        // время, за которое ток должен достичь минимума
  bal: integer;
  Freq: integer;


    /// settings
    I: integer;
    stACState: string;
    ACStateDostr: TStringList;
    kImax: Extended;
    kImin: Extended;
    bImax: Extended;
    bImin: Extended;
    isIncrement:boolean;
    isDecrement:boolean;
    isAC:boolean;

  end;

  { TDCState = (
    NullState = 'XXXXXXX',                   // 7o
    AC_I_increment = '111110X',         //  111110X
    AC_PWM_start = '011X1XX',           //  011X1XX
    AC_GasOpen_ResetT = 'XX1X0XX',      //  XX1X0XX
    AC_I_decrement_ResetT1 = 'XX01XXX', //  XX01XXX
    AC_GasClose = '0X001X1',            //  0X001X1
    AC_PWM_stop = '1X00XXX'             //  1X00XXX
    ); }

const
  stNullState =              'XXXXXXXXX'; // 7o
  stAC_I_increment =         '111110XXX'; // 111110X
  stAC_PWM_start =           '011X1XXXX'; // 011X1XX
  stAC_GasOpen_ResetT =      'XX1X0XXXX'; // XX1X0XX
  stAC_I_decrement_ResetT1 = 'XX01XXXXX'; // XX01XXX
  stAC_GasClose =            '0X001X1XX'; // 0X001X1
  stAC_PWM_stop =            '1X0XXXX1X'; // 1X00XXX
  stAC_AC_start =            '1XX1XXXX0';
  // stAC_PWM_stop =            '1X00XXX';            //  1X00XXX
  Ic_Start = 4;

implementation

uses TerminalUnit;

{ TButtonThread }

procedure TButtonThread_TIGAC.AC_AC_start;
begin
  startAC;
  isAC:= true;
end;

procedure TButtonThread_TIGAC.AC_I_increment;
var TV_Iabs, I1:integer;
begin
  // t:=0;
  if I < I_Min then
    I:= I_Min;

  if isIncrement = false then
  begin
    t_I:= round((I - bImax) / kImax); // расчет текущего положения на оси в соответствии с током I
    TV_Iabs:= TV_.tv_sec*1000000 + TV_.tv_usec;
    TV_Iabs:= TV_Iabs - t_I;
    TV_I.tv_sec:= TV_Iabs div 1000000;
    TV_I.tv_usec:= TV_Iabs - (TV_I.tv_sec)*1000000;
  end;

  isIncrement:=true;
  isDecrement:=false;

  I:= round(t_I * kImax + bImax);
  if I > I_Max then
    I:= I_Max;

SetI(I);
  I1:= I;
  if I < I_Min_Clean then
    I1:= I_Min_Clean;

  if I > I_Max_Clean then
    I1:= I_Max_Clean;

 SetIClean(I1);
end;

procedure TButtonThread_TIGAC.AC_PWM_start;
begin
StartPWM;
  isPWM:= true;
end;

procedure TButtonThread_TIGAC.AC_GasOpen_ResetT;
begin
 OpenGas;
  isGas:= 1;
  t:= 0;
  gettimeofday(TV, nil);
end;

procedure TButtonThread_TIGAC.AC_I_decrement_ResetT1;
var TV_Iabs:integer;
I1:integer;
begin
  if I > I_Max then
    I:= I_Max;

  if isDecrement = false then
  begin
    t_I:= round((I - bImin) / kImin); // расчет текущего положения на оси в соответствии с током I
    TV_Iabs:= TV_.tv_sec*1000000 + TV_.tv_usec;
    TV_Iabs:= TV_Iabs - t_I;
    TV_I.tv_sec:= TV_Iabs div 1000000;
    TV_I.tv_usec:= TV_Iabs - (TV_I.tv_sec)*1000000;
  end;

  isIncrement:=false;
  isDecrement:=true;

  I:= round(t_I * kImin + bImin);
  if I < I_Min then
    I:= I_Min;

 SetI(I);
  I1:= I;
  if I < I_Min_Clean then
    I1:= I_Min_Clean;

  if I > I_Max_Clean then
    I1:= I_Max_Clean;
SetIClean(I1);

  t1:= 0;
  gettimeofday(TV1, nil);
end;

procedure TButtonThread_TIGAC.AC_GasClose;
begin
  CloseGas;
  isGas:= 0;
end;

procedure TButtonThread_TIGAC.AC_PWM_stop;
begin
  StopPWM;

  isPWM:= false;
  isAC:= false;
end;

procedure TButtonThread_TIGAC.Execute;

  procedure parseParams(const str: string; var Ic: integer; var U: integer; var isBut: integer);
  var
    I, spos, sInd: integer;
    tmpstr: string;
    strArray: TStringList;
  begin
    strArray:= TStringList.Create;
    strArray.Delimiter:= ' ';
    strArray.StrictDelimiter:= true;
    strArray.DelimitedText:= str;
    if strArray.Count > 2 then
    begin
      Ic:= strtoint(strArray[0]);
      U:= strtoint(strArray[1]);
      isBut:= strtoint(strArray[2]);
    end
    else
    begin
      Ic:= -1;
      U:= -1;
      isBut:= -1;
    end;
  end;

  procedure getTVdiv;
  begin
    gettimeofday(TV_, nil);
    t:= (TV_.tv_sec * 1000000 + TV_.tv_usec) - (TV.tv_sec * 1000000 + TV.tv_usec);
    t1:= (TV_.tv_sec * 1000000 + TV_.tv_usec) - (TV1.tv_sec * 1000000 + TV1.tv_usec);
    t_I:= (TV_.tv_sec * 1000000 + TV_.tv_usec) - (TV_I.tv_sec * 1000000 + TV_I.tv_usec);
    { if tvdiv < 0 then
      tvdiv:= 1000000 + tvdiv; }
  end;

  function compareState(const stACState: string; const CalculatedState: string): boolean;
  var
    I: integer;
  begin
    for I:= 0 to length(stACState) - 1 do
      if (stACState[I] <> 'X') and (stACState[I] <> CalculatedState[I]) then
        break;
    if I = length(stACState) then
      result:= true
    else
      result:= false;
  end;

  procedure DoState(const CalculatedState: string);
  var
    key: string;
    I: integer;
  begin
    // for key in DCStateDo.Keys do
    for I:= 0 to ACStateDostr.Count - 1 do
    begin
      // DCStateDo.TryGetValue(Key, StateProc);
      key:= ACStateDostr[I];
      if (compareState(key, CalculatedState)) then
      begin
        if (key = stAC_I_increment) then          AC_I_increment;
        if (key = stAC_PWM_start) then            AC_PWM_start;
        if (key = stAC_GasOpen_ResetT) then       AC_GasOpen_ResetT;
        if (key = stAC_I_decrement_ResetT1) then  AC_I_decrement_ResetT1;
        if (key = stAC_GasClose) then             AC_GasClose;
        if (key = stAC_PWM_stop) then             AC_PWM_stop;
        if (key = stAC_AC_Start) then             AC_AC_Start;
        // DCStateDo.Items[key];
        //ReadAnsw;
      end;
    end;

  end;

  procedure setstate;
  begin
    DCState:= AC_I_increment;
  end;

begin
//  ACStateDo:= TDictionary<string, TACStateProc>.Create;
//
//  ACStateDo.Add(stAC_I_increment, AC_I_increment);
//  ACStateDo.Add(stAC_PWM_start, AC_PWM_start);
//  ACStateDo.Add(stAC_GasOpen_ResetT, AC_GasOpen_ResetT);
//  ACStateDo.Add(stAC_I_decrement_ResetT1, AC_I_decrement_ResetT1);
//  ACStateDo.Add(stAC_GasClose, AC_GasClose);
//  ACStateDo.Add(stAC_PWM_stop, AC_PWM_stop);

  ACStateDostr:= TStringList.Create;
  ACStateDostr.Add(stAC_I_increment);
  ACStateDostr.Add(stAC_PWM_start);
  ACStateDostr.Add(stAC_GasOpen_ResetT);
  ACStateDostr.Add(stAC_I_decrement_ResetT1);
  ACStateDostr.Add(stAC_GasClose);
  ACStateDostr.Add(stAC_PWM_stop);
  ACStateDostr.Add(stAC_AC_Start);

  isIncrement:=false;
  isDecrement:=false;

 // DoState(stAC_GasOpen_ResetT);

  answer:= '';
  cmdStatus:= false;
  isGas1:= 0;
  isGas:= 0;
  isPWM:= false;
  isAC:= false;

//  dI:= 1;
//  dI1:= 1;
  I:= 10;
  I_Min_clean:= 10;

 sendCommand('a');
//  form1.sendCommand('a');                 // инициализация режима TIG AC
  //ReadAnsw;
  synchronize(checkParams);

  SetBal(Bal);
  //ReadAnsw;
  SetFreq(Freq);
  //ReadAnsw;

  SetI(10);
  //ReadAnsw;
  SetIClean(10);
  //ReadAnsw;
  CloseGas;

  //ReadAnsw;
  gettimeofday(TV, nil);
  gettimeofday(TV1, nil);
  gettimeofday(TV_I, nil);

  while not Terminated do
    with form1 do
    begin
      synchronize(checkParams);

      answer:= sendCommand('l');


      //ReadAnsw;
      if (answer<>'read timeout')and(pos('nvalid',answer)<=0) then
      begin
        parseParams(answer, Ic, U, isBut);

        getTVdiv; // считаем t и t1

        stACState:= stNullState; // вычисляем "слово-состояние"
        if isPWM then          stACState[0]:= '1'   else stACState[0]:= '0';
        if t >= tStartPWM then stACState[1]:= '1'   else stACState[1]:= 'X'; //   500000
        if isBut > 0 then      stACState[2]:= '1'   else stACState[2]:= '0';
        if Ic >= 4 then        stACState[3]:= '1'   else stACState[3]:= '0';
        if isGas > 0 then      stACState[4]:= '1'   else stACState[4]:= '0';
        if I >= I_Max then     stACState[5]:= 'X'   else stACState[5]:= '0'; //   27
        if t1 > tCloseGas then stACState[6]:= '1'   else stACState[6]:= 'X'; //   6000000
        if I <= I_Min then     stACState[7]:= '1'   else stACState[7]:= 'X'; //   5
        if isAC then           stACState[8]:= 'X'   else stACState[8]:= '0';

        DoState(stACState);
      end;
      sleep(10);
//        synchronize(
//        procedure
//        begin
//          form1.stateWord.text:= stACState;
//        end
//        );
    end;
    sendCommand('q');
    Synchronize(form1.IDC.Disconnect);
end;


procedure TButtonThread_TIGAC.CheckParams;
begin
    if ChangeParamsFlag then
    with form1 do
    begin
      I_Min:= round(tb_Imin.Value);
      I_Max:= round(tb_Imax.Value);
      tCloseGas:= round(tb_tCloseGas.Value*1000000);    // время, через которое выключается газ при минимальном токе
      tStartPWM:= round(tb_tStartPWM.Value*1000000);     // время, через которое стартует ШИМ
      tImax:= round(tb_tImax.Value*1000000);        // время, за которое ток должен достичь максимума
      tImin:= round(tb_tIMin.Value*1000000);        // время, за которое ток должен достичь минимума
      kImax:= (I_max - I_min) / (tImax);
      kImin:= (I_min - I_max) / (tImin);
      bImax:= I_min - kImax*0;
      bImin:= I_max - kImin*0;
      I_Max_clean:= round(tb_Iclean.Value);
      Bal:= round(tb_Bal.Value);
      Freq:= round(tb_Freq.Value);

      ChangeParamsFlag:=false;
    end;
end;


procedure TButtonThread_TIGAC.ReadAnsw;
begin
  answer:= form1.readanswer;
end;

procedure TButtonThread_TIGAC.SetBal(Bal: integer);
begin
  sendCommand('b'+inttostr(Bal));
end;

procedure TButtonThread_TIGAC.SetFreq(Freq: integer);
begin
  sendCommand('f'+inttostr(Freq));
end;

procedure TButtonThread_TIGAC.SetI(I: integer);
begin
  sendCommand('a'+inttostr(I));
end;

procedure TButtonThread_TIGAC.SetIClean(I: integer);
begin
  sendCommand('c'+inttostr(I));
end;

procedure TButtonThread_TIGAC.StartPWM;
begin
  sendCommand('s');
end;

procedure TButtonThread_TIGAC.StartAC;
begin
  sendCommand('~');
end;

procedure TButtonThread_TIGAC.StopPWM;
begin
  sendCommand('e');
end;

procedure TButtonThread_TIGAC.CloseGas;
begin
  sendCommand('r');
end;

procedure TButtonThread_TIGAC.OpenGas;
begin
  sendCommand('g');
end;

function TButtonThread_TIGAC.sendCommand(cmd: String): String;
var res:string;
begin
  synchronize(
  procedure
  begin
    res:= form1.sendCommand(cmd);
  end
  );
  result:= res;
end;

end.
